//
// Created by Sasha Komarov on 16.12.2022.
//


#include "../include/bmp.h"
#include <stdlib.h>

static int64_t padding(uint64_t width) {
    return (int64_t)width % 4;
}

struct image createImageFrame(uint64_t height, uint64_t width) {
    struct image image;
    image.height = height;
    image.width = width;
    image.data = malloc(sizeof(struct pixel) * height * (width + padding(width)));
    return image;
}

struct image readImage(struct image inputImage, FILE *bmp){
    struct image image;
    image = inputImage;
    for (size_t i = 0; i < inputImage.height; i++) {
        fread(&(image.data[i * inputImage.width]), sizeof(struct pixel), inputImage.width, bmp);
        fseek(bmp, padding(inputImage.width), SEEK_CUR);
    }
    return image;
}

enum read_status makeInputImage(const char *filename, struct image *const inputImage) {
    enum read_status status;
    FILE *input = fopen(filename, "rb");
    if(!input){
        return READ_INVALID_FILE;
    }
    status = from_bmp(input, inputImage);
    return status;
}

size_t writeImage(FILE *out, struct image* img) {
    size_t written_bytes = 0;
    for (size_t i = 0; i < img->height; i++) {
        written_bytes += fwrite(&(img->data[i * img->width]), sizeof(struct pixel), img->width, out);
        fseek(out, padding(img->width), SEEK_CUR);
    }
    return written_bytes;
}

static FILE *create_file(const char *filename) {
    FILE *output = fopen(filename, "wb");
    return output;
}

enum write_status makeOutputImage(const char *filename, struct image *const outputImage) {
    enum write_status status;
    FILE *output = create_file(filename);
    if(!output){
        return WRITE_INVALID_FILE;
    }
    status = to_bmp(output, outputImage);
    return status;
}
