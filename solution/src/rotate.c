//
// Created by Sasha Komarov on 17.12.2022.
//

#include "../include/rotate.h"

static uint64_t oldPixelIndex(struct image image, int64_t i, int64_t j){
    return (image.width - 1) + j * image.width - i;
}

static uint64_t newPixelIndex(struct image const* image, int64_t i, int64_t j){
    return j + i * (image->width);
}

struct image rotate(struct image const* image){

    struct image outputImage;
    outputImage = createImageFrame(image->width, image->height);
    for (int64_t i = 0; i < image->height; i++) {
        for (int64_t j = 0; j < image->width; j++) {
            outputImage.data[oldPixelIndex(outputImage, i, j)] = image->data[newPixelIndex(image, i, j)];
        }
    }
    return outputImage;
}
