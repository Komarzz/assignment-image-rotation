//
// Created by Sasha Komarov on 16.12.2022.
//

#include "../include/bmp.h"
#include <stdio.h>
#include <stdlib.h>

#define BISIZE 40
#define BIPLANES 1
#define BIBITCOUNT 24
#define BFTYPE 0x4D42

struct bmpheader {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
}__attribute__((packed));


static int64_t padding(uint64_t width) {
    return (int64_t)width % 4;
}

static enum read_status read_bmpHeader(FILE *inputBmp, struct bmpheader* header) {
    if (feof(inputBmp)) {
        return READ_INVALID_SIGNATURE;
    }
    fread(header, sizeof(struct bmpheader), 1, inputBmp);
    if (ferror(inputBmp)) {
        return READ_INVALID_HEADER;
    }
    if (header->bfType != BFTYPE){
        return READ_INVALID_FILE;
    }
    return READ_OK;

}

enum read_status from_bmp(FILE *input, struct image *image) {
    struct bmpheader input_bmpHeader;
    enum read_status status;
    struct image tempImage;
    status = read_bmpHeader(input, &input_bmpHeader);
    if (status == READ_OK) {
        int64_t err = fseek(input, input_bmpHeader.bOffBits, SEEK_SET);
        if(err != 0){
            return READ_INVALID_FILE;
        }
        *image = createImageFrame(input_bmpHeader.biHeight, input_bmpHeader.biWidth);
        tempImage = readImage(*image, input);
        if(!tempImage.data){
            free(image->data);
            image->data = NULL;
            return READ_INVALID_BITS;
        }
        *image = tempImage;
        return READ_OK;
    }
    free(image->data);
    image->data = NULL;
    return status;
}

static enum write_status write_bmpHeader(FILE *output, uint64_t width, uint64_t height) {
    const struct bmpheader header = {
            .bfType = BFTYPE,
            .bfileSize = sizeof(struct bmpheader) + height * width * sizeof(struct pixel) + height * (padding(width)),
            .bOffBits = sizeof(struct bmpheader),
            .biSize = BISIZE,
            .biWidth = width,
            .biHeight = height,
            .biPlanes = BIPLANES,
            .biBitCount = BIBITCOUNT,
            .biSizeImage = height * width * sizeof(struct pixel) + padding(width) * height,
    };
    size_t written_bytes = fwrite(&header, sizeof(struct bmpheader), 1, output);
    if (written_bytes == 0){
        return WRITE_ERROR;
    }
    return WRITE_OK;
}

enum write_status to_bmp(FILE *output, struct image *const outputImage) {
    enum write_status status;
    size_t written_bytes;
    status = write_bmpHeader(output, outputImage->width, outputImage->height);
    if(status == WRITE_OK) {
        fseek(output, sizeof(struct bmpheader), SEEK_SET);
        written_bytes = writeImage(output, outputImage);
        if(written_bytes == 0){
            status = WRITE_ERROR;
        }
    }
    return status;
}





