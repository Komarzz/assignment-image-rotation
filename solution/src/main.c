#include "../include/image.h"
#include "../include/rotate.h"

int main(int argc, char **argv) {

    (void) argc;
    (void) argv;


    struct image inputImage;
    struct image outputImage;

    enum read_status readStatus;
    enum write_status writeStatus;

    readStatus = makeInputImage(argv[1],&inputImage);
    if(readStatus == READ_OK) {
        outputImage = rotate(&inputImage);
        free(inputImage.data);
        inputImage.data = NULL;
    }

    writeStatus = makeOutputImage(argv[2], &outputImage);
    if(writeStatus == WRITE_OK) {
        free(outputImage.data);
        outputImage.data = NULL;
    }


    return 0;
}

