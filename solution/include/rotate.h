//
// Created by Sasha Komarov on 17.12.2022.
//

#ifndef IMAGE_TRANSFORMER_ROTATE_H
#define IMAGE_TRANSFORMER_ROTATE_H

#include "bmp.h"

struct image rotate(struct image const* image);

#endif //IMAGE_TRANSFORMER_ROTATE_H
