//
// Created by Sasha Komarov on 16.12.2022.
//

#ifndef IMAGE_TRANSFORMER_BMP_H
#define IMAGE_TRANSFORMER_BMP_H

#include "image.h"

//struct BMPHEADER {
//    uint16_t bfType;
//    uint32_t bfileSize;
//    uint32_t bfReserved;
//    uint32_t bOffBits;
//    uint32_t biSize;
//    uint32_t biWidth;
//    uint32_t biHeight;
//    uint16_t biPlanes;
//    uint16_t biBitCount;
//    uint32_t biCompression;
//    uint32_t biSizeImage;
//    uint32_t biXPelsPerMeter;
//    uint32_t biYPelsPerMeter;
//    uint32_t biClrUsed;
//    uint32_t biClrImportant;
//}__attribute__((packed));

enum read_status {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_INVALID_FILE
};

enum write_status {
    WRITE_OK = 0,
    WRITE_ERROR,
    WRITE_INVALID_FILE
};

enum read_status from_bmp(FILE *input, struct image * image);

//enum read_status read_bmpHeader(FILE *inputBmp, struct BMPHEADER* header);

//enum write_status write_bmpHeader(FILE *output, uint64_t width, uint64_t height);

//FILE *create_bmp(const char *filename);

enum write_status to_bmp(FILE *output, struct image *const outputImage);

enum read_status makeInputImage(const char *filename, struct image *const inputImage);

enum write_status makeOutputImage(const char *filename, struct image *const outputImage);

#endif //IMAGE_TRANSFORMER_BMP_H
