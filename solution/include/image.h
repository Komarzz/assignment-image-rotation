//
// Created by Sasha Komarov on 16.12.2022.
//

#ifndef IMAGE_TRANSFORMER_IMAGE_H
#define IMAGE_TRANSFORMER_IMAGE_H

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

struct pixel {
    uint8_t b, g, r;
};

struct image {
    uint64_t width, height;
    struct pixel *data;
};

struct image readImage(struct image inputImage, FILE *bmp);

struct image createImageFrame(uint64_t height, uint64_t width);

size_t writeImage(FILE *out, struct image* img);

#endif //IMAGE_TRANSFORMER_IMAGE_H
